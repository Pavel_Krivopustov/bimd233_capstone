var fastFoodData = [
  {
    foodName: "Burger King, Whopper Sandwich",
    imageLink: "image/burgerKing.png",
    calories: "660",
	totalFat: "40g",
    sodium: "980mg",
    totalCarb: "49g",
    protein: "28g"
  },
  {
    foodName: "Jack in the Box, Jumbo Jack Cheeseburger",
    imageLink: "image/jackInTheBox.png",
    calories: "600",
	totalFat: "40g",
    sodium: "990mg",
    totalCarb: "33g",
    protein: "28g"
  },
  {
    foodName: "McDonald's, Big Breakfast with Hotcakes",
    imageLink: "image/mcDonalds.png",
    calories: "1350",
	totalFat: "65g",
    sodium: "2100mg",
    totalCarb: "155g",
    protein: "35g"
  },
  {
    foodName: "Taco Bell, Quesadilla",
    imageLink: "image/tacoBell.png",
    calories: "510",
	totalFat: "28g",
    sodium: "1210mg",
    totalCarb: "38g",
    protein: "27g"
  },
  {
    foodName: "Wendys, Spicy Chicken Sandwich",
    imageLink: "image/wendys.png",
    calories: "510",
	totalFat: "21g",
    sodium: "1110mg",
    totalCarb: "51g",
    protein: "30g"
  }
];

var alternativesData = [
  {
    foodName: "Chick-fil-A, Grilled Chicken Cool Wrap",
    imageLink: "image/CFA.png",
    calories: "350",
	totalFat: "14g",
    sodium: "960mg",
    totalCarb: "29g",
    protein: "37g"
  },
  {
    foodName: "Chipotle, Chicken Burrito Bowl",
    imageLink: "image/chipotle.png",
    calories: "500",
	totalFat: "13.5g",
    sodium: "805mg",
    totalCarb: "57g",
    protein: "42g"
  },
  {
    foodName: "Panera, Smoked Turkey Breast Sandwich on Country",
    imageLink: "image/panera.png",
    calories: "430",
	totalFat: "3.5g",
    sodium: "1790mg",
    totalCarb: "67g",
    protein: "33g"
  },
  {
    foodName: "Starbucks, Spinach and Feta Wrap",
    imageLink: "image/starbucks.png",
    calories: "290",
	totalFat: "10g",
    sodium: "830mg",
    totalCarb: "33g",
    protein: "19g"
  },
  {
    foodName: "Subway, 6-inch Turkey Breast Sub",
    imageLink: "image/subway.png",
    calories: "340",
	totalFat: "9g",
    sodium: "670mg",
    totalCarb: "49g",
    protein: "18g"
  }
];

var el = document.getElementById("fastFoodTable");
var el2 = document.getElementById("healthyAlternativesTable");
var i;
for (i = 0; i < fastFoodData.length; i++) {
  el.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="fastFoodTool"> <img src=' +
    fastFoodData[i]["imageLink"] +
    ' width= 200px alt="Fast Food"></a></td><td>';
  var theTitle = getArrayTitle(fastFoodData, i);
  document.getElementsByClassName("fastFoodTool")[i].title = theTitle; 
}

for (i = 0; i < alternativesData.length; i++) {
  el2.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="alternativesTool"> <img src=' +
    alternativesData[i]["imageLink"] +
    ' width= 200px alt="Healthy Fast Food Alternatives"></a></td><td>';
  var theTitle = getArrayTitle(alternativesData, i);
  document.getElementsByClassName("alternativesTool")[i].title = theTitle;
}

/* Print the title for the tooltip to display the data for the item */
function getArrayTitle(arrayData, index) {
  var fullTitle = "";
  fullTitle += arrayData[i]["foodName"] + "\n";
  fullTitle += "\nCalories: " + arrayData[i]["calories"];
  fullTitle += "\nTotal Fat: " + arrayData[i]["totalFat"];
  fullTitle += "\nSodium: " + arrayData[i]["sodium"];
  fullTitle += "\nTotal Carbohydrates: " + arrayData[i]["totalCarb"];
  fullTitle += "\nProtein: " + arrayData[i]["protein"];
  return fullTitle;
}

/* Display the styled tooltip near the item the user is hovering over */
$(".fastFoodTool").tooltip();
$(".alternativesTool").tooltip();

/* Change background color based on what section the user is viewing */
$(function() {
  $(window).on("scroll", function() {
    var height = document.getElementById("firstWell").scrollHeight;
    height -= 50;
    if ($(window).scrollTop() > height) {
      $("body").css("background-color", "white");
    } else {
      $("body").css("background-color", "black");
    }
  });
});
