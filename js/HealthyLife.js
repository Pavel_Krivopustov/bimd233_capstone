sleepBeforeShow();
function sleepBeforeShow() {
  window.setTimeout(showWelcomeMessage, 1000);
}

function showWelcomeMessage() {
  $(document).ready(function() {
    $("div.hidden")
      .fadeIn(3000)
      .removeClass("hidden");
  });
}
