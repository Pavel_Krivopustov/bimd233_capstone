var candyData = [
  {
    candyName: "3 Musketeers",
    imageLink: "image/3musketeersBar.png",
    servingSize: "1 bar (54.4g)",
    calories: "240",
    totalFat: "7g",
    satFat: "5g",
    transFat: "0g",
    cholest: "5mg",
    sodium: "90mg",
    totalCarb: "42g",
    dietaryFiber: "1g",
    sugars: "36g",
    protein: "1g"
  },
  {
    candyName: "Butterfinger",
    imageLink: "image/butterFingerBar.png",
    servingSize: "1 bar (54g)",
    calories: "250",
    totalFat: "10g",
    satFat: "5g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "100mg",
    totalCarb: "39g",
    dietaryFiber: "1g",
    sugars: "24g",
    protein: "3g"
  },
  {
    candyName: "Kit Kat",
    imageLink: "image/kitKatBar.png",
    servingSize: "1 package (42g)",
    calories: "210",
    totalFat: "11g",
    satFat: "7g",
    transFat: "0g",
    cholest: "5mg",
    sodium: "30mg",
    totalCarb: "27g",
    dietaryFiber: "1g",
    sugars: "22g",
    protein: "3g"
  },
  {
    candyName: "Milky Way",
    imageLink: "image/milkyWayBar.png",
    servingSize: "1 bar (52.2g)",
    calories: "240",
    totalFat: "9g",
    satFat: "7g",
    transFat: "0g",
    cholest: "10mg",
    sodium: "75mg",
    totalCarb: "37g",
    dietaryFiber: "1g",
    sugars: "31g",
    protein: "2g"
  },
  {
    candyName: "Snickers",
    imageLink: "image/snickersBar.png",
    servingSize: "1 bar (52.7g)",
    calories: "250",
    totalFat: "12g",
    satFat: "4.5g",
    transFat: "0g",
    cholest: "5mg",
    sodium: "120mg",
    totalCarb: "33g",
    dietaryFiber: "1g",
    sugars: "27g",
    protein: "4g"
  },
  {
    candyName: "Twix",
    imageLink: "image/twixBar.png",
    servingSize: "2 cookies (50.7g)",
    calories: "250",
    totalFat: "12g",
    satFat: "7g",
    transFat: "0g",
    cholest: "5mg",
    sodium: "100mg",
    totalCarb: "34g",
    dietaryFiber: "1g",
    sugars: "24g",
    protein: "2g"
  }
];

var alternativesData = [
  {
    fruitName: "Apple",
    imageLink: "image/apple.png",
    servingSize: "1 Medium Apple (182g / 6.4oz)",
    calories: "95",
    totalFat: "0g",
    satFat: "0g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "2mg",
    totalCarb: "25g",
    dietaryFiber: "4g",
    sugars: "19g",
    protein: "0g"
  },
  {
    fruitName: "Apricot",
    imageLink: "image/apricot.png",
    servingSize: "1 Large Apricot",
    calories: "17",
    totalFat: "0g",
    satFat: "0g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "0mg",
    totalCarb: "4g",
    dietaryFiber: "1g",
    sugars: "3g",
    protein: "0g"
  },
  {
    fruitName: "Nectarine",
    imageLink: "image/nectarine.png",
    servingSize: "1 Large Nectarine",
    calories: "69",
    totalFat: "0g",
    satFat: "0g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "0mg",
    totalCarb: "16g",
    dietaryFiber: "3g",
    sugars: "12g",
    protein: "2g"
  },
  {
    fruitName: "Peach",
    imageLink: "image/peach.png",
    servingSize: "1 Large Peach",
    calories: "68",
    totalFat: "0g",
    satFat: "0g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "0mg",
    totalCarb: "17g",
    dietaryFiber: "3g",
    sugars: "15g",
    protein: "2g"
  },
  {
    chocolateName: "Ghirardelli Intense Dark",
    imageLink: "image/ghirardelli.png",
    servingSize: "4 sections (45g)",
    servingsPerContainer: "2",
    calories: "250",
    totalFat: "25g",
    satFat: "15g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "0mg",
    totalCarb: "15g",
    dietaryFiber: "5g",
    sugars: "5g",
    protein: "3g"
  },
  {
    chocolateName: "Lindt",
    imageLink: "image/lindt.png",
    servingSize: "4 squares (40g)",
    servingsPerContainer: "2.5",
    calories: "240",
    totalFat: "22g",
    satFat: "13g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "10mg",
    totalCarb: "12g",
    dietaryFiber: "5g",
    sugars: "3g",
    protein: "4g"
  },
  {
    chocolateName: "Pascha",
    imageLink: "image/pascha.png",
    servingSize: "10 pieces (42g)",
    servingsPerContainer: "about 2.5",
    calories: "260",
    totalFat: "19g",
    satFat: "12g",
    transFat: "0g",
    cholest: "0mg",
    sodium: "0mg",
    totalCarb: "16g",
    dietaryFiber: "4g",
    sugars: "7g",
    protein: "5g"
  }
];

var el = document.getElementById("candyTable");
var el2 = document.getElementById("healthyAlternativesTable");
var i;
for (i = 0; i < candyData.length; i++) {
  el.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="candyTool"> <img src=' +
    candyData[i]["imageLink"] +
    ' width= 200px alt="Candy Bar"></a></td><td>';
  var theTitle = getArrayTitle(candyData, i);
  document.getElementsByClassName("candyTool")[i].title = theTitle;
}

for (i = 0; i < alternativesData.length; i++) {
  el2.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="alternativesTool"> <img src=' +
    alternativesData[i]["imageLink"] +
    ' width= 200px alt="Healthy Alternatives"></a></td><td>';
  var theTitle = getArrayTitle(alternativesData, i);
  document.getElementsByClassName("alternativesTool")[i].title = theTitle;
}

/* Print the title for the tooltip to display the data for the item */
function getArrayTitle(arrayData, index) {
  var fullTitle = "";
  fullTitle += "Serving Size: " + arrayData[i]["servingSize"];
  if (Object.keys(arrayData[index]).length == 14) {
    fullTitle += "\nServings Per Container: " + arrayData[i]["servingsPerContainer"];
  }
  fullTitle += "\nCalories: " + arrayData[i]["calories"];
  fullTitle += "\nTotal Fat: " + arrayData[i]["totalFat"];
  fullTitle += "\n   Saturated Fat: " + arrayData[i]["satFat"];
  fullTitle += "\n   Trans Fat: " + arrayData[i]["transFat"];
  fullTitle += "\nCholesterol: " + arrayData[i]["cholest"];
  fullTitle += "\nSodium: " + arrayData[i]["sodium"];
  fullTitle += "\nTotal Carbohydrates: " + arrayData[i]["totalCarb"];
  fullTitle += "\n   Dietary Fiber: " + arrayData[i]["dietaryFiber"];
  fullTitle += "\n   Sugars: " + arrayData[i]["sugars"];
  fullTitle += "\nProtein: " + arrayData[i]["protein"];
  return fullTitle;
}

/* Display the styled tooltip near the item the user is hovering over */
$(".candyTool").tooltip();
$(".alternativesTool").tooltip();

/* Change background color based on what section the user is viewing */
$(function() {
  $(window).on("scroll", function() {
    var height = document.getElementById("firstWell").scrollHeight;
    height -= 50;
    if ($(window).scrollTop() > height) {
      $("body").css("background-color", "white");
    } else {
      $("body").css("background-color", "black");
    }
  });
});
