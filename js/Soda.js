var sodaData = [
  {
    drinkName: "Coca-Cola",
    imageLink: "image/cocaCola.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "140",
    totalFat: "0g",
    sodium: "45mg",
    totalCarb: "39g",
    sugars: "39g",
    protein: "0g"
  },
  {
    drinkName: "Dr Pepper",
    imageLink: "image/drPepper.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "150",
    totalFat: "0g",
    sodium: "55mg",
    totalCarb: "40g",
    sugars: "40g",
    protein: "0g"
  },
  {
    drinkName: "Fanta",
    imageLink: "image/fanta.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "160",
    totalFat: "0g",
    sodium: "60mg",
    totalCarb: "45g",
    sugars: "44g",
    protein: "0g"
  },
  {
    drinkName: "Mountain Dew",
    imageLink: "image/mountainDew.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "170",
    totalFat: "0g",
    sodium: "60mg",
    totalCarb: "46g",
    sugars: "46g",
    protein: "0g"
  },
  {
    drinkName: "Pepsi",
    imageLink: "image/pepsi.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "150",
    totalFat: "0g",
    sodium: "30mg",
    totalCarb: "41g",
    sugars: "41g",
    protein: "0g"
  },
  {
    drinkName: "Mug Root Beer",
    imageLink: "image/mugRootBeer.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "160",
    totalFat: "0g",
    sodium: "65mg",
    totalCarb: "43g",
    sugars: "43g",
    protein: "0g"
  },
  {
    drinkName: "Sprite",
    imageLink: "image/sprite.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "140",
    totalFat: "0g",
    sodium: "65mg",
    totalCarb: "38g",
    sugars: "38g",
    protein: "0g"
  }
];

var alternativesData = [
  {
    drinkName: "Aloe Gloe, 15% Organic Coconut Water",
    imageLink: "image/aloeGloe.png",
    servingSize: "8 fl oz (240mL)",
	servingsPerContainer: "About 2 (15.2 fl oz)",
    calories: "25",
    totalFat: "0g",
    sodium: "15mg",
    totalCarb: "7g",
    sugars: "6g",
    protein: "0g"
  },
  {
    drinkName: "Bolthouse, Berry Boost",
    imageLink: "image/bolthouse.png",
    servingSize: "8 fl oz (240mL)",
    servingsPerContainer: "About 2 (15.2 fl oz)",
    calories: "120",
    totalFat: "0g",
    sodium: "15mg",
    totalCarb: "27g",
    sugars: "21g",
    protein: "1g"
  },
  {
    drinkName: "Glaceau Smartwater Sparkling",
    imageLink: "image/smartWater.png",
    servingSize: "1 Bottle",
    calories: "0",
    totalFat: "0g",
    sodium: "0mg",
    totalCarb: "0g",
    sugars: "0g",
    protein: "0g"
  },
  {
    drinkName: "GT's Kombucha, Heart Beet",
    imageLink: "image/kombucha.png",
    servingSize: "8 fl oz (240mL)",
	servingsPerContainer: "About 2 (16.2 fl oz)",
    calories: "30",
    totalFat: "0g",
    sodium: "10mg",
    totalCarb: "8g",
    sugars: "8g",
    protein: "0g"
  },
  {
    drinkName: "Izze Sparkling Juice, Peach",
    imageLink: "image/izze.png",
    servingSize: "8.4 fl oz (248mL)",
    servingsPerContainer: "1",
    calories: "90",
    totalFat: "0g",
    sodium: "15mg",
    totalCarb: "21g",
    sugars: "19g",
    protein: "0g"
  },
  {
    drinkName: "La Croix, Berry",
    imageLink: "image/laCroix.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "0",
    totalFat: "0g",
    sodium: "0mg",
    totalCarb: "0g",
    sugars: "0g",
    protein: "0g"
  },
  {
    drinkName: "Spindrift, Cucumber",
    imageLink: "image/spindrift.png",
    servingSize: "12 fl oz (355 mL)",
    calories: "2",
    totalFat: "0g",
    sodium: "0mg",
    totalCarb: "0g",
    sugars: "0g",
    protein: "0g"
  }
];

var el = document.getElementById("sodaTable");
var el2 = document.getElementById("healthyAlternativesTable");
var i;
for (i = 0; i < sodaData.length; i++) {
  el.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="sodaTool"> <img src=' +
    sodaData[i]["imageLink"] +
    ' width= 200px alt="Soda"></a></td><td>';
  var theTitle = getArrayTitle(sodaData, i);
  document.getElementsByClassName("sodaTool")[i].title = theTitle;
}

for (i = 0; i < alternativesData.length; i++) {
  el2.innerHTML +=
    '<tr><td align="center"><a data-placement="right" rel="tooltip" class="alternativesTool"> <img src=' +
    alternativesData[i]["imageLink"] +
    ' width= 200px alt="Healthy Soda Alternatives"></a></td><td>';
  var theTitle = getArrayTitle(alternativesData, i);
  document.getElementsByClassName("alternativesTool")[i].title = theTitle;
}

/* Print the title for the tooltip to display the data for the item */
function getArrayTitle(arrayData, index) {
  var fullTitle = "";
  fullTitle += arrayData[i]["drinkName"] + "\n";
  fullTitle += "\nServing Size: " + arrayData[i]["servingSize"];
  if (Object.keys(arrayData[index]).length == 10) {
    fullTitle += "\nServings Per Container: " + arrayData[i]["servingsPerContainer"];
  }
  fullTitle += "\nCalories: " + arrayData[i]["calories"];
  fullTitle += "\nTotal Fat: " + arrayData[i]["totalFat"];
  fullTitle += "\nSodium: " + arrayData[i]["sodium"];
  fullTitle += "\nTotal Carbohydrates: " + arrayData[i]["totalCarb"];
  fullTitle += "\n   Sugars: " + arrayData[i]["sugars"];
  fullTitle += "\nProtein: " + arrayData[i]["protein"];
  return fullTitle;
}

/* Display the styled tooltip near the item the user is hovering over */
$(".sodaTool").tooltip();
$(".alternativesTool").tooltip();

/* Change background color based on what section the user is viewing */
$(function() {
  $(window).on("scroll", function() {
    var height = document.getElementById("firstWell").scrollHeight;
    height -= 50;
    if ($(window).scrollTop() > height) {
      $("body").css("background-color", "white");
    } else {
      $("body").css("background-color", "black");
    }
  });
});
