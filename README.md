# README #

This README documents the Capstone Project for BIMD 233.

### What is this repository for? ###

Showcase what I have learned over the course of the quarter.

### About Healthy Life ###

My final project focuses around physical health. The website shows various consumables that include candy, fast food, and soda. Hovering over the items will show the nutritional informaiton for that item. Healthier alternatives are shown at the bottom of each page.

Textual information and data was partially pulled from the following sources:

https://www.healthline.com/nutrition/20-foods-to-avoid-like-the-plague#section17
https://healthyeater.com/dark-chocolate-best-and-worst
http://www.businessinsider.com/healthy-meals-at-chipotle-panera-shake-shack-2016-4/#panera--smoked-turkey-breast-sandwich-on-country--430-calories-9

Nutritional facts were gathered from manufacturer's web site. When no data was available, I used Google images to find the nutrition label or crowdsource websites like http://www.myfitnesspal.com.